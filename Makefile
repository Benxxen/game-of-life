PROGRAM_NAME=gameoflife
BINARY_NAME=$(PROGRAM_NAME).bin

run:
	go run --mod=vendor ./gameoflife.go -file=grid.txt

test:
	go test ./... -v

build:
	go build -o $(BINARY_NAME) --mod=vendor .