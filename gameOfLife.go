package main

import (
	"flag"
	"os"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"
)

func main() {
	var generations []LifeBoard

	file := flag.String("file", "", "a string")
	heightOfBoard := flag.Int("height", 5, "height of the board")
	widthOfBoard := flag.Int("width", 8, "width of the board")

	flag.Parse()

	log.Info().Msgf("creating board with following specifications: width: %d, height: %d, from file: %s", *widthOfBoard, *heightOfBoard, *file)

	board := newBoard(*heightOfBoard, *widthOfBoard)

	fileContent, err := os.ReadFile(*file)
	if err != nil {
		log.Error().Err(err).Msgf("couldn´t read content from file: %s", *file)

		os.Exit(1)
	}

	err = board.fillBoardWithLife(fileContent)

	//Create next 5 generations
	for i := 0; i < 5; i++ {
		nextGen := board.createNextGeneration()
		generations = append(generations, *nextGen)
		board.cells = nextGen.cells
	}

	//Print out generations
	for v, generation := range generations {
		log.Info().Msgf("%d. Generation", v+1)

		for _, row := range generation.cells {
			log.Info().Msgf("%v", row)
		}
	}
}

type LifeBoard struct {
	cells  [][]int
	height int
	width  int
}

//newBoard returns a new LifeBoard.
func newBoard(height, width int) *LifeBoard {
	cells := make([][]int, height)
	for columns := range cells {
		cells[columns] = make([]int, width)
	}

	return &LifeBoard{
		width:  width,
		height: height,
		cells:  cells,
	}
}

//getCellHealthStatus sets the health status of the cell with the given coordinates on the board.
func (lb *LifeBoard) setCellHealthStatus(height, width, status int) {
	lb.cells[height][width] = status
}

//getCellHealthStatus returns the actual health status of the cell with the given coordinates on the board.
func (lb *LifeBoard) getCellHealthStatus(height, width int) int {
	//fmt.Println(height, width)
	return lb.cells[height][width]
}

//fillBoardWithLife sets the health status for all cells on the board depending on the byte input given.
func (lb *LifeBoard) fillBoardWithLife(content []byte) error {
	fileLines := strings.Split(string(content), "\n")
	for v, fileLine := range fileLines {
		gridLine := strings.Split(fileLine, ",")
		for w, cell := range gridLine {
			cellStatus, err := strconv.Atoi(cell)
			if err != nil {
				log.Error().Err(err).Msgf("couldn´t convert status: %s", cell)

				return err
			}

			lb.setCellHealthStatus(v, w, cellStatus)
		}
	}

	return nil
}

//getLivingNeighbours returns the number of living neighbours of the cell with the given coordinates on the board.
func (lb *LifeBoard) getLivingNeighbours(height, width int) int {
	livingNeighbours := 0

	for i := -1; i <= 1; i++ {
		for j := -1; j <= 1; j++ {

			if width+j < 0 || width+j > lb.width-1 || height+i < 0 || height+i > lb.height-1 || (i == 0 && j == 0) {
				continue
			}

			livingNeighbours += lb.getCellHealthStatus(height+i, width+j)
		}
	}

	return livingNeighbours
}

//calculateNextGenHealthStatus determines the health state of the cell on the next gen board.
//Following the rules below:

//1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
//2. Any live cell with more than three live neighbours dies, as if by overcrowding.
//3. Any live cell with two or three live neighbours lives on to the next generation.
//4. Any dead cell with exactly three live neighbours becomes a live cell.

func (lb *LifeBoard) calculateNextGenHealthStatus(height, width int) int {
	numberOfLivingNeighbours := lb.getLivingNeighbours(height, width)
	currenGentHealthStatus := lb.getCellHealthStatus(height, width)

	if (currenGentHealthStatus == 1 && (numberOfLivingNeighbours == 2 || numberOfLivingNeighbours == 3)) || (currenGentHealthStatus == 0 && numberOfLivingNeighbours == 3) {
		return 1
	}

	return 0
}

func (lb *LifeBoard) createNextGeneration() *LifeBoard {
	nextGenBoard := newBoard(lb.height, lb.width)
	for y, row := range lb.cells {
		for x, _ := range row {
			nextGenHealthStatus := lb.calculateNextGenHealthStatus(y, x)
			nextGenBoard.setCellHealthStatus(y, x, nextGenHealthStatus)
		}
	}

	return nextGenBoard
}
