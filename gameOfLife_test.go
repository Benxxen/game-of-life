package main

import (
	"reflect"
	"testing"
)

func TestFillBoardWithNewLife(t *testing.T) {
	input := []byte("0,1,0,0,0,1,0,0\n0,0,0,1,0,0,0,0\n0,1,0,0,0,0,0,0\n0,0,0,1,0,0,0,0\n0,1,0,0,0,0,0,0")
	board := newBoard(5, 8)
	expectedCells := [][]int{
		{0, 1, 0, 0, 0, 1, 0, 0},
		{0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 0, 0, 0},
	}

	err := board.fillBoardWithLife(input)

	if err != nil {
		t.Errorf("returned error while creating grid: %v, content: %v", err, input)
	} else {
		t.Logf("shouldn´t return any error")

	}

	if reflect.DeepEqual(expectedCells, board.cells) {
		t.Logf("result equals expected grid")
	} else {
		t.Errorf("result doesn´t equal expected grid: %v, got: %v", expectedCells, board.cells)
	}
}

func TestGetLivingNeighbours(t *testing.T) {
	board := newBoard(5, 8)
	board.cells = [][]int{
		{1, 1, 0, 0, 0, 1, 0, 0},
		{0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 0, 1},
		{0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 0, 1},
	}

	tts := []struct {
		name       string
		heightCell int

		widthCell                   int
		expNumberOfLivingNeighbours int
	}{
		{"living cell in the left upper corner", 0, 0, 1},
		{"dead cell in the middle", 2, 3, 2},
		{"dead cell in the right bottom corner", 4, 7, 0},
		{"dead cell in the left bottom corner", 4, 0, 1},
		{"dead cell in the right upper corner", 0, 7, 0},
		{"dead cell in the right bottom", 3, 6, 4},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			numberLivingNeighbours := board.getLivingNeighbours(tt.heightCell, tt.widthCell)

			if tt.expNumberOfLivingNeighbours != numberLivingNeighbours {
				t.Errorf("should equal expected number of living neighbours of: %d, got: %d", tt.expNumberOfLivingNeighbours, numberLivingNeighbours)
			} else {
				t.Logf("should equal expected number of living neighbours")
			}
		})
	}

}

func TestCalculateNextGenHealthStatus(t *testing.T) {
	tts := []struct {
		name       string
		cellHeight int
		cellWidth  int
		input      LifeBoard
		expHealth  int
	}{
		{"any live cell with fewer than two live neighbours dies", 0, 1, LifeBoard{cells: [][]int{{0, 1, 0, 0, 0}, {0, 0, 0, 1, 0}, {0, 1, 0, 0, 0}}, height: 3, width: 5}, 0},
		{"any live cell with more than three live neighbours dies", 1, 3, LifeBoard{cells: [][]int{{0, 1, 0, 1, 1}, {0, 0, 0, 1, 1}, {0, 1, 0, 1, 0}}, height: 3, width: 5}, 0},
		{"any live cell with two or three live neighbours lives on to the next generation", 1, 3, LifeBoard{cells: [][]int{{0, 1, 0, 0, 1}, {0, 0, 0, 1, 1}, {0, 1, 0, 1, 0}}, height: 3, width: 5}, 1},
		{"any dead cell with exactly three live neighbours becomes a live cell", 2, 2, LifeBoard{cells: [][]int{{0, 1, 0, 0, 1}, {0, 0, 0, 1, 1}, {0, 1, 0, 1, 0}}, height: 3, width: 5}, 1},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			nextGenHealth := tt.input.calculateNextGenHealthStatus(tt.cellHeight, tt.cellWidth)
			if nextGenHealth != tt.expHealth {
				t.Errorf("should equal expected cell heatlh: %d, but got: %d", tt.expHealth, nextGenHealth)
			} else {
				t.Logf("should equal expted cell health")
			}
		})
	}
}

func TestCreateNextGeneration(t *testing.T) {
	tts := []struct {
		name      string
		input     LifeBoard
		expResult [][]int
		err       error
	}{
		{"normal case", LifeBoard{cells: [][]int{{0, 1, 0, 0, 0}, {0, 0, 0, 1, 0}, {0, 1, 0, 0, 0}}, height: 3, width: 5}, [][]int{{0, 0, 0, 0, 0}, {0, 0, 1, 0, 0}, {0, 0, 0, 0, 0}}, nil},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			nextGenBoard := tt.input.createNextGeneration()

			if reflect.DeepEqual(nextGenBoard.cells, tt.expResult) {
				t.Logf("should equal expected generation of cells")
			} else {
				t.Errorf("should equal expected generation of cells: %v, but got: %v", tt.expResult, nextGenBoard.cells)
			}
		})
	}

}
