# Game of Life

## How to run it

### Run application with existing grid.txt
```
make run
```

### Run with own file
```
go run --mod=vendor ./gameoflife.go -file=(path/to/file)
```

### Build application && run with file
```
make build && ./gameoflife.bin -file=grid.txt
```

### Run tests
```
make test
```
